
TRP/TIS test program for LoRa concentrator
==========================================

1. Introduction
---------------

This software is used to test TRP/TIS with antenna chamber.
Its peer node is LoRa node for mbed.

2. Usage
--------

See command line help to get the list of all available options:
./util_trp_tis -h

Example:
./util_trp_tis

