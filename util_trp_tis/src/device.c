/*
 * Description:
 *	3D Antenna Power & Sensitivity Test Chamber
 *
 * License: Revised BSD License, see LICENSE.TXT file include in the project
 */
#include <stdio.h>
#include <wiringPi.h>

#define GPIO_RESET	7

/* not defined yet */
#define GPIO_ATT_SERIN	1
#define GPIO_ATT_CLK	2
#define GPIO_ATT_LE	3

#define MAX_ATT		6
#define ATT_BITS	(6*6)

int dev_init(void)
{
    wiringPiSetupGpio();

    pinMode(GPIO_RESET, OUTPUT);
    digitalWrite(GPIO_RESET, 1);
    delay(1);
    digitalWrite(GPIO_RESET, 0);

    pinMode(GPIO_ATT_SERIN, OUTPUT);
    pinMode(GPIO_ATT_CLK, OUTPUT);
    pinMode(GPIO_ATT_LE, OUTPUT);

    digitalWrite(GPIO_ATT_SERIN, 0);
    digitalWrite(GPIO_ATT_CLK, 0);
    digitalWrite(GPIO_ATT_LE, 0);
    return 0;
}


int dev_att_set_raw(int *val)
{
    int i, j;

    digitalWrite(GPIO_ATT_SERIN, 0);
    digitalWrite(GPIO_ATT_CLK, 0);
    digitalWrite(GPIO_ATT_LE, 0);

    for (i = 0; i < MAX_ATT; i++) {
	for (j = 5; j >= 0; j--) {
	    /* revese bit for meeiting HCM624 input truth table  */
	    digitalWrite(GPIO_ATT_SERIN, (val[i] & (1 << j)) ? 0 : 1);
	    digitalWrite(GPIO_ATT_CLK, 1);
	    delayMicroseconds(1);
	    digitalWrite(GPIO_ATT_CLK, 0);
	    delayMicroseconds(1);
	}
    }

    digitalWrite(GPIO_ATT_LE, 1);
    delayMicroseconds(1);
    digitalWrite(GPIO_ATT_LE, 0);
    delayMicroseconds(1);
    return 0;
}

int dev_att_set(int att_2x)
{
    int i; 
    int val[MAX_ATT];

    for (i = 0; i < MAX_ATT; i++) {
	int v;
	if (att_2x > 0)
	    v = att_2x & 0x3f;
	else 
	    v = 0;

	val[i] = v;
	att_2x -= v;
    }
    
    dev_att_set_raw(val);
    return 0;
}
